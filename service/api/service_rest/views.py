from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from django.http import JsonResponse
from .models import Technician, Appointment, AutomobileVO
import json


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        'id',
        'name',
        'employee_number',
    ]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "vin",
        "color",
        "year",
        "vip"
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "vin",
        "owner",
        "date",
        "time",
        "technician",
        "reason",
        "status"
    ]
    encoders = {
        "technician": TechnicianEncoder(),
        "vin": AutomobileVOEncoder()
    }

    def get_extra_data(self, o):
        return {"status": o.status.name}


@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {'technicians': technicians},
            encoder=TechnicianEncoder,
        )

    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                    technician,
                    encoder=TechnicianEncoder,
                    safe=False,
                )
        except:
            return JsonResponse(
                {'message': 'Could not create the technician'},
                status=400,
            )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_technician(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )

        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status=400,
            )

    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=pk)
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status=400,
            )

    else: 
        # else if this request method is PUT, then run the try/except block
        try:
            content = json.loads(request.body)
            Technician.objects.filter(id=pk).update(**content)
            technician = Technician.objects.get(id=pk)
            # why cant combine 78 and 79 as one like:
            # technician = Technician.object.filter(id=pk).update(**content)?
            # Insominia will return 1 in preview and not the whole object data
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )

        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status=400,
            )


@require_http_methods(["GET", "POST"])
def api_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {'appointments': appointments},
            encoder=AppointmentEncoder,
        )

    else:
        content = json.loads(request.body)
        try:
            auto_vin = content["vin"]
            # print("AUTOOO", auto_vin)
            auto = AutomobileVO.objects.get(vin=auto_vin)
            print("@#$@#$", auto)
            content['vin'] = auto
            print("VIVIAN", auto)
            tech = Technician.objects.get(id=content['technician'])
            content['technician'] = tech
            # print("!@#", content['technician'])

        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {'message': "Could not create the appointment"},
                status=400,
            )

        # appointment = Appointment.objects.create(**content)
        appointment = Appointment.create(**content)
        # print("^&%^#$^#%$", appointment)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_appointment(request, pk):
    if request.method == "GET":
        try:

            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                    {'appointments': appointment},
                    encoder=AppointmentEncoder,
                )

        except Appointment.DoesNotExist:
            return JsonResponse(
                {'message': "Does not exist"},
                status=400,
            )

    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.delete()
            return JsonResponse(
                    appointment,
                    encoder=AppointmentEncoder,
                    safe=False,
                )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status=400,
            )

    else:

        try:
            content = json.loads(request.body)
            Appointment.objects.filter(id=pk).update(**content)
            # print("123", content)
            appointment = Appointment.objects.get(id=pk)
            # print("222", appointment)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )

        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status=400,
            )
